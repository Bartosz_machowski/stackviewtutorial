//
//  ConfigurableView.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 10/10/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import Foundation
import UIKit

enum State {
    case active
    case denied
    case neutral
}

class ConfigurableView: UIView {
    private let constraint: CGFloat = 50
    private var image: UIImage?

    var state: State {
        didSet {
            updateState()
        }
    }

    private var defaultConfiguration: [State: UIColor] = [.neutral: .lightGray,
                                                          .active: .yellow,
                                                          .denied: .purple]

    private lazy var displayView: UIView = {
        let defaultImageValue = UIView(frame: .zero)
        defaultImageValue.backgroundColor = .black

        return defaultImageValue
    }()

    var configuration: [State: UIColor]

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.backgroundColor = .red
        imageView.image = image
        imageView.contentMode = .center

        return imageView
    }()

    init(state: State, image: UIImage?, configuration: [State: UIColor] = [:] ) {
        self.configuration = configuration
        self.state = state
        self.image = image
        super.init(frame: .zero)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        self.backgroundColor = .blue
        if image != nil {
            addSubview(imageView)
            addConstraints(for: imageView, containerView: self)
            refreshImageColor()
        } else {
            addSubview(displayView)
            addConstraints(for: displayView, containerView: self)
            refreshViewColor()
        }
    }

    func updateState() {
        if image == nil {
            refreshViewColor()
        } else {
            refreshImageColor()
        }
    }

    func refreshImageColor() {
        imageView.tintColor = colorForCurrentState()
    }

    func refreshViewColor() {
        displayView.backgroundColor = colorForCurrentState()
    }

    func colorForCurrentState() -> UIColor {
        guard let color = configuration[state] else { return defaultConfiguration[state]! }
        return color
    }

    private func addConstraints(for insideView: UIView, containerView: UIView) {
        insideView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: constraint).isActive = true
        insideView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -constraint).isActive = true
        insideView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: constraint).isActive = true
        insideView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -constraint).isActive = true
        insideView.translatesAutoresizingMaskIntoConstraints = false
    }
}

