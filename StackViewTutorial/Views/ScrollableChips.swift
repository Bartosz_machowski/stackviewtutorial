//
//  ScrollableChips.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 26/09/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import UIKit

class ScrollableChips: UIView {
    lazy private var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    lazy private var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .green
        return scrollView
    }()

    var buttonsArray: [UIButton] = []
    var labelsArray: [UILabel] = []
    let constraint: CGFloat = 24
    var color: UIColor
    var fontColor: UIColor
    var selectionHandler: ((String) -> ())?

    init(color: UIColor, fontColor: UIColor) {
        self.color = color
        self.fontColor = fontColor
        super.init(frame: .zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }



    // MARK: - functions

    func setup(chips: [String]) {
        backgroundColor = .red
        addSubview(scrollView)
        scrollView.addSubview(stackView)

        addConstraints(for: scrollView, containerView: self)
        addConstraints(for: stackView, containerView: scrollView)

        addButtonsToStackView(buttons: makeButtonsWithText(strings: chips))
    }

    @objc func buttonClicked(_ sender: UIButton) {
        let selectedButtonTitle = sender.titleLabel?.text ?? ""
        selectionHandler?(selectedButtonTitle)
    }

    // MARK: - preparing subviews
    func makeButtonsWithText(strings: [String]) -> [UIButton]  {
        strings.forEach { string in
            let button = UIButton()
            button.setTitle(string, for: .normal)
            button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
            button.titleLabel?.numberOfLines = 0
            button.backgroundColor = .blue
            button.sizeThatFits(CGSize.zero)
            button.contentEdgeInsets = UIEdgeInsets(top: 16, left: 20, bottom: 16, right: 20)
            button.layer.cornerRadius = 25
            button.backgroundColor = color
            button.setTitleColor(fontColor, for: .normal)
            button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            button.layer.shadowOpacity = 1.0
            button.layer.shadowRadius = 0.0
            button.layer.masksToBounds = false
            buttonsArray.append(button)
            addSubview(button)
        }

        return buttonsArray
    }

    func addButtonsToStackView(buttons: [UIButton]) {
        buttons.forEach { button in
            stackView.addArrangedSubview(button)
        }
    }

    func addConstraints(for insideView: UIView, containerView: UIView) {
        insideView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: constraint).isActive = true
        insideView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -constraint).isActive = true
        insideView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: constraint).isActive = true
        insideView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: constraint).isActive = true
    }
}
