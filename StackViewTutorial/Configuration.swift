//
//  Configuration.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 10/10/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import Foundation
import UIKit


enum Api: String {
    case domain
    case path
    case queryKey

    var string: String {
        switch self {
        case .domain:
            return "https://private-4c7c4b-vorwerk1.apiary-mock.com"
        case .path:
            return "/quick_replies"
        case .queryKey:
            return "text"
        }
    }
}

enum Colors {
    static let chipsColor = UIColor.green
    static let primaryColor = UIColor.white
    static let chipsFontColor = UIColor.yellow
}
