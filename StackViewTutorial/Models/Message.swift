//
//  Message.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 15/10/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import Foundation

struct Response: Codable {
    var text: String?
    var quickReplies: [QuickReplies]

    enum CodingKeys: String, CodingKey {
        case facebook = "_facebook"
    }

    enum FacebookKeys: String, CodingKey {
        case message
    }

    enum MessageKeys: String, CodingKey {
        case text
        case quickReplies = "quick_replies"
    }

    struct QuickReplies: Codable {
        var contentType: String?
        var title: String!
        var payload: String?
        var imageUrl: String?

        enum QuickRepliesKeys: String, CodingKey {
            case contentType = "content_type"
            case title
            case payload
            case imageUrl = "image_url"
        }
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let facebookContainer = try container.nestedContainer(keyedBy: FacebookKeys.self, forKey: .facebook)
        let messageContainer = try facebookContainer.nestedContainer(keyedBy: MessageKeys.self, forKey: .message)
        text = try messageContainer.decode(String?.self, forKey: .text)
        quickReplies = try messageContainer.decode([QuickReplies].self, forKey: .quickReplies)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var facebookContainer = container.nestedContainer(keyedBy: FacebookKeys.self, forKey: .facebook)
        var messageContainer = facebookContainer.nestedContainer(keyedBy: MessageKeys.self, forKey: .message)
        try messageContainer.encode(text, forKey: .text)
        try messageContainer.encode(quickReplies, forKey: .quickReplies)
    }
}
