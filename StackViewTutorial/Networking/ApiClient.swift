//
//  ApiClient.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 15/10/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import Foundation

class ApiClient {
    func request<T>(url: URL, success: @escaping ((T) -> Void), failure: @escaping ((Error?) -> Void)) where T : Codable {
        let urlSession = URLSession.shared

        let task = urlSession.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    let jsonData = try JSONDecoder().decode(T.self, from: data)
                    success(jsonData)
                } catch {
                    print("Unexpected error: \(error).")
                }
            }
        }
        task.resume()
    }

    func performGetRequest(searchTerm: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        var urlComponents = URLComponents(string: Api.domain.string)
        urlComponents?.path = Api.path.string
        urlComponents?.queryItems = [URLQueryItem(name: Api.queryKey.string, value: searchTerm)]

        guard let url = urlComponents?.url else { return }

        let task = session.dataTask(with: url) { data, response, error in

            guard error == nil else {
                print ("error: \(error!)")
                return
            }

            guard let content = data else {
                print("No data")
                return
            }

            guard let json = (try? JSONSerialization.jsonObject(with: content,
                                                                options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                                                                    print("Not containing JSON")
                                                                    return
            }

            print("gotten json response dictionary is \n \(json)")
        }

        task.resume()
    }
}

