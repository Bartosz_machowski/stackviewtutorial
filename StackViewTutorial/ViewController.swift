//
//  ViewController.swift
//  StackViewTutorial
//
//  Created by bartosz.machowski on 24/09/2018.
//  Copyright © 2018 bartosz.machowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let scrollableChips = ScrollableChips(color: .yellow, fontColor: .blue)
    let configurableView = ConfigurableView(state: .neutral, image: nil)
    var textField = UITextField()
    var confirmButton = UIButton()
    var stringsArray: [String] = []
    private var apiClient = ApiClient()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        setupChips()
        setupTextField()
        setupConfirmButton()
        view.backgroundColor = .yellow
    }

    private func loadData() {
        var urlComponents = URLComponents(string: Api.domain.string)
        urlComponents?.path = Api.path.string
        guard let url = urlComponents?.url else { return }

        self.apiClient.request(url: url,
                               success: { [weak self] (response: Response) in
            for text in response.quickReplies {
                DispatchQueue.main.async {
                    self?.stringsArray = [text.title]
                    self?.refreshArray()
                }
            }
            }, failure: { error in
                print("Can not fetch data.")
        })
    }

    func refreshArray() {
        scrollableChips.setup(chips: stringsArray)
    }

    func setupChips() {
        view.addSubview(scrollableChips)

        setScrollableChipsConstraints()
        scrollableChips.setup(chips: stringsArray)
        scrollableChips.selectionHandler = { chip in
            print("We are inside closure. \(chip)")
        }
    }

    func setupConfigurableView() {
        view.addSubview(configurableView)

        setConfigurableViewConstraints()
    }

    func setupTextField() {
        textField = UITextField(frame: CGRect(x: 36, y: 200, width: 300, height: 40))
        textField.placeholder = "Enter text here"
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        textField.clearButtonMode = UITextField.ViewMode.whileEditing
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        self.view.addSubview(textField)
    }

    func setScrollableChipsConstraints() {
        scrollableChips.translatesAutoresizingMaskIntoConstraints = false
        scrollableChips.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollableChips.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollableChips.trailingAnchor.constraint(equalTo:  view.trailingAnchor).isActive = true
        scrollableChips.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    func setConfigurableViewConstraints() {
        configurableView.translatesAutoresizingMaskIntoConstraints = false
        configurableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        configurableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        configurableView.trailingAnchor.constraint(equalTo:  view.trailingAnchor).isActive = true
        configurableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    func setupConfirmButton() {
        confirmButton = UIButton(frame: CGRect(x: 130, y: 250, width: 100, height: 30))
        confirmButton.setTitle("OK", for: .normal)
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked(_:)), for: .touchUpInside)
        confirmButton.titleLabel?.numberOfLines = 0
        confirmButton.backgroundColor = .blue
        confirmButton.sizeThatFits(CGSize.zero)
        confirmButton.contentEdgeInsets = UIEdgeInsets(top: 16, left: 20, bottom: 16, right: 20)
        confirmButton.layer.cornerRadius = 10
        confirmButton.backgroundColor = .blue
        self.view.addSubview(confirmButton)
    }

    @objc func confirmButtonClicked(_ sender: UIButton) {
        if !textField.text!.isEmpty {
            apiClient.performGetRequest(searchTerm: textField.text!)
        }

    }
}

